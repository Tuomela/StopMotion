Windows 10 Stopmotion
================

Stopmotion is an uwp application to create stop motion videos by taking pictures
with webcamera.


INSTALLATION
------------
You will need visual studio 2015 or later.

-open solution with visual studio

-compile and run


If you want to build a market place package,
see https://docs.microsoft.com/en-us/windows/uwp/packaging/packaging-uwp-apps


WHO CAN USE IT
--------------
Everyone can use this application as it is distributed under the
MIT licence