﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.Graphics.Imaging;
using Windows.Media;
using Windows.Media.Capture;
using Windows.Media.Core;
using Windows.Media.Editing;
using Windows.Media.MediaProperties;
using Windows.Media.Transcoding;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.System.Display;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media.Imaging;

namespace Stopmotion.Models
{
    public class Capturer
    {
        private const string _tempPhotoName = "capture{0}.jpg";
        public ObservableCollection<StorageFile> Frames { get; set; }

        public int Index = 0;
        int fileIndex = 0;

        public int FrameDelay = 400;

        MediaCapture mediaCapture;
        bool isPreviewing;

        DisplayRequest displayRequest = new DisplayRequest();

        Dictionary<int, StorageFile> Files = new Dictionary<int, StorageFile>();

        public Capturer()
        {
            Frames = new ObservableCollection<StorageFile>();
        }

        public async Task Clear(CoreDispatcher dispatcher)
        {
            await CleanupCameraAsync(dispatcher);

            for (int n = Frames.Count - 1; n >= 0; n--)
            {
                await RemoveFrame(n);
            }
        }

        public async Task<MediaCapture> InitializePreview()
        {
            try
            {

                mediaCapture = new MediaCapture();
                await mediaCapture.InitializeAsync();

                displayRequest.RequestActive();
                DisplayInformation.AutoRotationPreferences = DisplayOrientations.Landscape;
            }
            catch (UnauthorizedAccessException)
            {
                // This will be thrown if the user denied access to the camera in privacy settings
                await new MessageDialog("Camera is not enabled for this application.").ShowAsync();
                return null;
            }
            catch (COMException)
            {
                await new MessageDialog("No web camera found.").ShowAsync();
                return null;
            }

            return mediaCapture;
        }

        public async Task StartPreviewAsync()
        {
            try
            {
                if (!isPreviewing)
                {
                    await mediaCapture.StartPreviewAsync();
                    isPreviewing = true;
                }
            }
            catch (System.IO.FileLoadException)
            {
            }
        }

        public async Task RemoveFrame(int n)
        {
            if (Files.ContainsKey(n))
            {
                await Files[n].DeleteAsync();
                Files.Remove(n);
                bool removeMax = false;
                for(int m=n + 1; Files.ContainsKey(m); m++)
                {
                    Files[m - 1] = Files[m];
                    removeMax = true;
                }

                if(removeMax)
                    Files.Remove(Files.Keys.Max());
            }
            ArrangeFrames();
        }

        private async Task<MediaComposition> GetComposition(int frameDelay)
        {
            MediaComposition composition = new MediaComposition();
            foreach (StorageFile file in Frames)
            {
                var clip = await MediaClip.CreateFromImageFileAsync(file, TimeSpan.FromMilliseconds(frameDelay));
                composition.Clips.Add(clip);
            }
            return composition;
        }

        public async Task<BitmapImage> GetFrameSource(int index)
        {
            if(index >= 0 && index < Frames.Count)
            {
                BitmapImage bitmapImage = new BitmapImage();
                using (FileRandomAccessStream stream = (FileRandomAccessStream)await Frames[index].OpenAsync(FileAccessMode.Read)) {
                    bitmapImage.SetSource(stream);
                }
                return bitmapImage;
            }
            else
            {
                return null;
            }
        }

        public async Task<MediaSource> GetPreviewVideo(int w, int h)
        {
            MediaStreamSource mediaStreamSource = (await GetComposition(FrameDelay)).GeneratePreviewMediaStreamSource(w, h);
            return MediaSource.CreateFromMediaStreamSource(mediaStreamSource);
        }

        public async Task<SoftwareBitmapSource> GetPicture()
        {
            if (mediaCapture != null)
            {

                // Get information about the preview
                var previewProperties = mediaCapture.VideoDeviceController.GetMediaStreamProperties(MediaStreamType.VideoPreview) as VideoEncodingProperties;

                // Create a video frame in the desired format for the preview frame
                VideoFrame videoFrame = new VideoFrame(BitmapPixelFormat.Bgra8, (int)previewProperties.Width, (int)previewProperties.Height);
                VideoFrame previewFrame = await mediaCapture.GetPreviewFrameAsync(videoFrame);

                SoftwareBitmap previewBitmap = previewFrame.SoftwareBitmap;
                var source = new SoftwareBitmapSource();
                await source.SetBitmapAsync(previewBitmap);

                StorageFile file = await CreateTempImage();
                if (Files.ContainsKey(Index))
                {
                    for (int n = Files.Keys.Max(); n >= Index; n--)
                        Files[n + 1] = Files[n];
                }
                Files[Index] = file;

                previewFrame.Dispose();
                previewFrame = null;

                Index++;

                ArrangeFrames();

                return source;
            }else
            {
                return null;
            }
        }

        private async Task<StorageFile> CreateTempImage()
        {
            StorageFolder folder = Windows.Storage.ApplicationData.Current.LocalFolder;
            StorageFile file = await folder.CreateFileAsync(string.Format(_tempPhotoName, fileIndex), CreationCollisionOption.ReplaceExisting);
            fileIndex++;

            using (var captureStream = new InMemoryRandomAccessStream())
            {
                await mediaCapture.CapturePhotoToStreamAsync(ImageEncodingProperties.CreateJpeg(), captureStream);

                using (var fileStream = await file.OpenAsync(FileAccessMode.ReadWrite))
                {
                    var decoder = await BitmapDecoder.CreateAsync(captureStream);
                    var encoder = await BitmapEncoder.CreateForTranscodingAsync(fileStream, decoder);

                    var properties = new BitmapPropertySet { { "System.Photo.Orientation", new BitmapTypedValue(PhotoOrientation.Normal, PropertyType.UInt16) } };
                    await encoder.BitmapProperties.SetPropertiesAsync(properties);

                    await encoder.FlushAsync();
                }
            }

            return file;
        }

        public async Task CleanupCameraAsync(CoreDispatcher dispatcher)
        {
            if (mediaCapture != null)
            {
                if (isPreviewing)
                {
                    await mediaCapture.StopPreviewAsync();
                    isPreviewing = false;
                }

                await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    if (displayRequest != null)
                    {
                        displayRequest.RequestRelease();
                    }

                    mediaCapture.Dispose();
                    mediaCapture = null;
                });
            }

        }

        private void ArrangeFrames()
        {
            for(int n=0; (Files.Keys.Count > 0 && n <= Files.Keys.Max()) || n < Frames.Count; n++)
            {
                if (Files.ContainsKey(n))
                {
                    if (Frames.Count > n && Frames[n] != Files[n])
                        Frames[n] = Files[n];
                    else if (Frames.Count <= n)
                        Frames.Add(Files[n]);
                }else if(Frames.Count > n)
                {
                    Frames.RemoveAt(n);
                    n--;
                }
            }
        }

        public async Task RenderCompositionToFile(Action<double> progressCallback, Action<TranscodeFailureReason, AsyncStatus> completed)
        {
            var picker = new Windows.Storage.Pickers.FileSavePicker();
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.VideosLibrary;
            picker.FileTypeChoices.Add("MP4 files", new List<string>() { ".mp4" });
            picker.SuggestedFileName = "RenderedComposition.mp4";

            Windows.Storage.StorageFile file = await picker.PickSaveFileAsync();
            if (file != null)
            {
                // Call RenderToFileAsync
                var saveOperation = (await GetComposition(FrameDelay)).RenderToFileAsync(file, MediaTrimmingPreference.Precise);

                saveOperation.Progress = new AsyncOperationProgressHandler<TranscodeFailureReason, double>(async (info, progress) =>
                {
                    progressCallback?.Invoke(progress);
                });
                saveOperation.Completed = new AsyncOperationWithProgressCompletedHandler<TranscodeFailureReason, double>(async (info, status) =>
                {
                    completed?.Invoke(info.GetResults(), status);
                });
            }
        }
    }
}
