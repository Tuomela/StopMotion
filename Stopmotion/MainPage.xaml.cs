﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.Media.Transcoding;
using Windows.Storage.FileProperties;
using Stopmotion.Models;
using Windows.UI;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Stopmotion
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page, IDisposable
    {
        private Capturer capturer;

        public MainPage()
        {
            this.InitializeComponent();

            Application.Current.Resources["SystemControlHighlightListAccentLowBrush"] = new SolidColorBrush(Colors.Green);
            Application.Current.Resources["SystemControlHighlightListAccentMediumBrush"] = new SolidColorBrush(Colors.Green);

            Application.Current.Suspending += Application_Suspending;
            Application.Current.Resuming += Application_Resuming;
        }

        public async void Dispose()
        {
            if (capturer != null)
            {
                await capturer.Clear(Dispatcher);
                capturer = null;
            }
        }

        public async void Application_Suspending(object sender, SuspendingEventArgs e)
        {
            // Handle global application events only if this page is active
            if (Frame.CurrentSourcePageType == typeof(MainPage) &&
                capturer != null)
            {
                var deferral = e.SuspendingOperation.GetDeferral();
                PreviewControl.Source = null;
                await capturer.CleanupCameraAsync(Dispatcher);
                deferral.Complete();
            }
        }

        private async void Application_Resuming(object sender, object e)
        {
            if(capturer != null)
            {
                ProgressGrid.Visibility = Visibility.Visible;
                ProgressText.Text = "";
                await Task.Delay(200);
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    PreviewControl.Source = await capturer.InitializePreview();
                    await capturer.StartPreviewAsync();
                    ProgressGrid.Visibility = Visibility.Collapsed;
                    CaptureBtn.IsEnabled = true;
                });
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Init();
        }

        private async void Init()
        {
            if (capturer == null)
            {
                capturer = new Capturer();
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    PreviewControl.Source = await capturer.InitializePreview();
                    if (PreviewControl.Source == null) Application.Current.Exit();

                    await capturer.StartPreviewAsync();
                });
                DataContext = capturer;
            }
        }
 

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            Dispose();
        }

        
        private async void Capture_Click(object sender, RoutedEventArgs e)
        {
            CaptureBtn.IsEnabled = false;
            LastImage.Source = await capturer.GetPicture();
            FramesList.SelectedIndex = capturer.Index - 1;

            await Task.Delay(200);

            GenerateVideoBtn.IsEnabled = capturer.Frames.Count > 0;
            CaptureBtn.IsEnabled = true;
        }

        private async void GenerateVideoBtn_Click(object sender, RoutedEventArgs e)
        {
            if (capturer.Frames.Count > 0)
            {
                capturer.FrameDelay = (int)DelaySlider.Value;
                MediaPlayer.Visibility = Visibility.Visible;
                EditorButtons.Visibility = Visibility.Collapsed;
                PlayerButtons.Visibility = Visibility.Visible;
                EditorGrid.Visibility = Visibility.Collapsed;
                FramesGrid.Visibility = Visibility.Collapsed;
                ProgressGrid.Visibility = Visibility.Visible;
                ProgressText.Text = "";

                await Task.Delay(100);

                MediaPlayer.Source = await capturer.GetPreviewVideo(
                (int)MediaPlayer.ActualWidth,
                (int)MediaPlayer.ActualHeight);

                ProgressGrid.Visibility = Visibility.Collapsed;
            }
        }

        private async void SaveVideoBtn_Click(object sender, RoutedEventArgs e)
        {
            ProgressGrid.Visibility = Visibility.Visible;
            await capturer.RenderCompositionToFile(async (progress) => await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, new DispatchedHandler(() =>
            {
                ProgressText.Text = string.Format("Saving file... Progress: {0:F0}%", progress);
            })),
            async (results, status) => await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, new DispatchedHandler(async () =>
            {
                try
                {
                    if (results != TranscodeFailureReason.None || status != AsyncStatus.Completed)
                    {
                        await new MessageDialog("Saving failed...").ShowAsync();
                    }
                    else
                    {
                        await new MessageDialog("Video generated successfully!").ShowAsync();
                    }
                }
                finally
                {
                    ProgressGrid.Visibility = Visibility.Collapsed;
                }

            }))
            );
        }

        private void BackToEditorBtn_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.Visibility = Visibility.Collapsed;
            EditorButtons.Visibility = Visibility.Visible;
            PlayerButtons.Visibility = Visibility.Collapsed;
            EditorGrid.Visibility = Visibility.Visible;
            FramesGrid.Visibility = Visibility.Visible;
        }

        private async void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(FramesList.SelectedIndex >= 0)
            {
                capturer.Index = FramesList.SelectedIndex + 1;
                LastImage.Source = await capturer.GetFrameSource(FramesList.SelectedIndex);
            }

            DeleteBtn.IsEnabled = FramesList.SelectedIndex >= 0;
        }

        private async void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            int index = FramesList.SelectedIndex;
            await capturer.RemoveFrame(index);
            await Task.Delay(100);
            capturer.Index = index;
            FramesList.SelectedIndex = index - 1;

            GenerateVideoBtn.IsEnabled = capturer.Frames.Count > 0;
        }

        private void ReplayBtn_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.MediaPlayer.PlaybackSession.Position = TimeSpan.FromMilliseconds(0);
            MediaPlayer.MediaPlayer.Play();
        }
    }

    public class FileToThumbnailConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            StorageFile file = value as StorageFile;
            if (file != null)
            {
                BitmapImage image = new BitmapImage();

                StorageItemThumbnail thumbnail = Task.Run(async () =>
                {
                    return await file.GetThumbnailAsync(
                    Windows.Storage.FileProperties.ThumbnailMode.PicturesView,
                    50,
                    Windows.Storage.FileProperties.ThumbnailOptions.UseCurrentScale);
                }).Result;

                using (Stream stream = thumbnail.AsStreamForRead())
                {
                    image.SetSource(stream.AsRandomAccessStream());
                    return image;
                }
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
